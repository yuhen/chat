-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 �?03 �?21 �?11:18
-- 服务器版本: 5.5.53
-- PHP 版本: 5.5.38

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `chat`
--

-- --------------------------------------------------------

--
-- 表的结构 `chat_admin`
--

CREATE TABLE IF NOT EXISTS `chat_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `pw` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL,
  `state` int(1) NOT NULL DEFAULT '0',
  `login_time` datetime NOT NULL,
  `login_ip` varchar(20) NOT NULL,
  `num` int(2) NOT NULL DEFAULT '0',
  `err_time` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `chat_admin`
--

INSERT INTO `chat_admin` (`id`, `name`, `pw`, `add_time`, `state`, `login_time`, `login_ip`, `num`, `err_time`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '0000-00-00 00:00:00', 0, '2019-03-20 03:11:51', '127.0.0.1', 0, 0),
(2, '1111111', '7fa8282ad93047a4d6fe6111c93b308a', '2019-01-08 03:20:39', 0, '0000-00-00 00:00:00', '0.0.0.0', 0, 0),
(3, '11111112', '7fa8282ad93047a4d6fe6111c93b308a', '2019-01-08 03:20:43', 0, '0000-00-00 00:00:00', '0.0.0.0', 0, 0),
(4, '111111', '96e79218965eb72c92a549dd5a330112', '2019-03-08 01:55:23', 0, '0000-00-00 00:00:00', '0.0.0.0', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `chat_auth_group`
--

CREATE TABLE IF NOT EXISTS `chat_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 : 1为正常,0为禁用',
  `rules` char(80) NOT NULL DEFAULT '' COMMENT '规则ID （这里填写的是 tp_auth_rule里面的规则的ID)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='后台用户组表' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `chat_auth_group`
--

INSERT INTO `chat_auth_group` (`id`, `title`, `status`, `rules`) VALUES
(1, '超级管理组', 1, '1'),
(2, 'hhhh', 1, '1,3');

-- --------------------------------------------------------

--
-- 表的结构 `chat_auth_group_access`
--

CREATE TABLE IF NOT EXISTS `chat_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限组规则表';

--
-- 转存表中的数据 `chat_auth_group_access`
--

INSERT INTO `chat_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2);

-- --------------------------------------------------------

--
-- 表的结构 `chat_auth_rule`
--

CREATE TABLE IF NOT EXISTS `chat_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(20) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `pid` smallint(5) unsigned NOT NULL COMMENT '父级ID',
  `icon` varchar(50) DEFAULT '' COMMENT '图标',
  `sort` tinyint(4) unsigned NOT NULL COMMENT '排序',
  `condition` char(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='后台功能表' AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `chat_auth_rule`
--

INSERT INTO `chat_auth_rule` (`id`, `name`, `title`, `type`, `status`, `pid`, `icon`, `sort`, `condition`) VALUES
(1, '', '管理员管理', 1, 1, 0, '&#xe68e', 100, ''),
(3, 'adminlist', '管理员列表', 1, 1, 1, '', 0, ''),
(2, 'groups', '用户组', 1, 1, 1, '', 0, ''),
(4, 'auth/index', '功能管理', 1, 1, 1, '', 0, ''),
(5, '', '客服管理', 1, 1, 0, '&#xe606;', 0, ''),
(6, '', '系统管理', 1, 1, 0, '&#xe614;', 0, ''),
(7, 'systems/config', '系统设置', 1, 1, 6, '', 0, ''),
(8, '3434', '3434', 1, 1, 6, '', 0, ''),
(9, 'chat/chatlist', '客服列表', 1, 1, 5, '', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `chat_chat_record`
--

CREATE TABLE IF NOT EXISTS `chat_chat_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` varchar(255) NOT NULL,
  `toid` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `time` varchar(30) NOT NULL,
  `isonline` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `foid` varchar(255) NOT NULL,
  `ftoid` varchar(255) NOT NULL,
  `state` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fid` (`fid`),
  KEY `toid` (`toid`),
  KEY `soid` (`foid`),
  KEY `ftoid` (`ftoid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `chat_chat_record`
--

INSERT INTO `chat_chat_record` (`id`, `fid`, `toid`, `content`, `time`, `isonline`, `type`, `foid`, `ftoid`, `state`) VALUES
(1, 'MTI3LjAuMC4xMTU1MjYzMDMxNw==', '00001', '[喵喵]', '2019-03-18 11:27:49', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1MjYzMDMxNw==', 0),
(2, 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', '00001', 'kkkk', '2019-03-18 11:28:09', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', 0),
(3, 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', '00001', 'lll', '2019-03-18 11:28:33', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', 0),
(4, 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', '00001', 'llp', '2019-03-18 11:29:18', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', 0),
(5, 'MTI3LjAuMC4xMTU1Mjg3OTc3Mw==', '00001', 'lll7777', '2019-03-18 11:29:38', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTc3Mw==', 1),
(6, 'MTI3LjAuMC4xMTU1Mjg3OTc3Mw==', '00001', '[[[[', '2019-03-18 11:29:51', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTc3Mw==', 1),
(7, 'MTI3LjAuMC4xMTU1Mjg4MDAzMg==', '00001', '[抓狂]', '2019-03-18 11:33:59', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg4MDAzMg==', 1),
(8, 'MTI3LjAuMC4xMTU1Mjg4MDAzMg==', '00001', '[坏笑]', '2019-03-18 11:34:07', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg4MDAzMg==', 1),
(9, 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', '00001', 'jjjj', '2019-03-20 16:45:47', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', 0),
(10, 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', '00001', 'ggg', '2019-03-20 16:49:57', 1, 'save', '00001', 'MTI3LjAuMC4xMTU1Mjg3OTY4NQ==', 0);

-- --------------------------------------------------------

--
-- 表的结构 `chat_chat_user`
--

CREATE TABLE IF NOT EXISTS `chat_chat_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `pw` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL,
  `state` int(1) NOT NULL DEFAULT '0',
  `login_time` datetime NOT NULL,
  `login_ip` varchar(20) NOT NULL,
  `num` int(2) NOT NULL DEFAULT '0',
  `err_time` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `chat_chat_user`
--

INSERT INTO `chat_chat_user` (`id`, `name`, `pw`, `add_time`, `state`, `login_time`, `login_ip`, `num`, `err_time`) VALUES
(2, '00001', '9437d68357534bd880bec09c1b8e21bd', '0000-00-00 00:00:00', 0, '2019-03-14 05:50:14', '127.0.0.1', 0, 1552556571),
(3, '00002', 'e10adc3949ba59abbe56e057f20f883e', '2019-03-20 11:13:00', 0, '0000-00-00 00:00:00', '0.0.0.0', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
